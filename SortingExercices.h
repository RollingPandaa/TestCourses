
#pragma once

#include <QObject>

namespace SortingExercices
{
Q_NAMESPACE

enum Algorithm
{
    QuickSort,
    MergeSort,
    IntroSort,
    HeapSort,
    InsertionSort,
    BlockSort,
    TimSort,
    SelectionSort,
    CubeSort,
    ShellSort,
    BubbleSort,
    ExchangeSort,
    TreeSort,
    CycleSort,
    LibrarySort,
    PatienceSort,
    SmoothSort,
    StrandSort,
    TournamentSort,
    ShakerSort,
    CombSort,
    GnomeSort,
    OddEvenSort,
    MinAlgo = QuickSort,
    MaxAlgo = OddEvenSort
};
Q_ENUM_NS(Algorithm)

void sortInPlace(int array[], int size, Algorithm algo);

void quickSort(int array[], int size);
void mergeSort(int array[], int size);
void introSort(int array[], int size);
void heapSort(int array[], int size);
void insertionSort(int array[], int size);
void blockSort(int array[], int size);
void timSort(int array[], int size);
void selectionSort(int array[], int size);
void cubeSort(int array[], int size);
void shellSort(int array[], int size);
void bubbleSort(int array[], int size);
void exchangeSort(int array[], int size);
void treeSort(int array[], int size);
void cycleSort(int array[], int size);
void librarySort(int array[], int size);
void patienceSort(int array[], int size);
void smoothSort(int array[], int size);
void strandSort(int array[], int size);
void tournamentSort(int array[], int size);
void shakerSort(int array[], int size);
void combSort(int array[], int size);
void gnomeSort(int array[], int size);
void oddEvenSort(int array[], int size);
};    // namespace SortingExercices
