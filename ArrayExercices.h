
#pragma once

#include <QObject>

namespace ArrayExercices
{
Q_NAMESPACE

/**
 * @brief Interveti dans le tableau les deux valeurs aux indices donnés
 * @param fromIndex Indice de la première valeur à échanger
 * @param toIndex Indice de la seconde valeur à échanger
 * @param array Tableau d'entiers
 * @param size Taille du tableau
 * @return vrai si l'échange a été effectué, faux sinon
 */
bool swapInPlace(int fromIndex, int toIndex, int array[], int size);

/**
 * @brief Recherche la première occurence d'une valeur dans un tableau
 * @param val Valeur à trouver dans le tableau
 * @param array Tableau d'entiers
 * @param size Taille du tableau
 * @return indice de la première occurrence de val dans array, ou -1 si aucune occurrence n'est trouvée
 */
int searchFirst(int val, int array[], int size);

/**
 * @brief Recherche la dernière occurence d'une valeur dans un tableau
 * @param val Valeur à trouver dans le tableau
 * @param array Tableau d'entiers
 * @param size Taille du tableau
 * @return indice de la dernière occurrence de val dans array, ou -1 si aucune occurrence n'est trouvée
 */
int searchLast(int val, int array[], int size);

/**
 * @brief Recherche une valeur dans un tableau, soit la première occurrence soit la dernière
 *        Si last est à vrai, recherche la dernière occurrence du tableau, sinon recherche la première occurrence
 * @param val Valeur à trouver dans le tableau
 * @param array Tableau d'entiers
 * @param size Taille du tableau
 * @param last Indique si l'on recherche la première ou la dernière occurrence
 * @return l'indice de l'occurrence trouvée, ou -1 si rien n'a été trouvé
 */
int search(int val, int array[], int size, bool last = false);

/**
 * @brief Supprime la première occurence d'une valeur donnée dans un tableau
 *        Les valeurs dans le tableau doivent rester contigues : les cases vides "déplacées" à la fin et remplies par des -1.
 * @param val Valeur à supprimer dans le tableau
 * @param array Tableau d'entiers
 * @param size Taille du tableau
 * @return indice de la valeur supprimée, ou -1 si aucune valeur n'a été supprimée
 */
int removeFirst(int val, int array[], int size);

/**
 * @brief Supprime toutes les occurences d'une valeur donnée dans un tableau
 *        Les valeurs dans le tableau doivent rester contigues : les cases vides "déplacées" à la fin et remplies par des -1.
 * @param val Valeur à supprimer dans le tableau
 * @param array Tableau d'entiers
 * @param size Taille du tableau
 * @return le nombre d'occurrences supprimées du tableau
 */
int removeAll(int val, int array[], int size);

/**
 * @brief Supprimer les doublons d'un tableau
 *        Pour chaque valeur apparaissant plusieurs fois dans le tableau, supprime les répétitions après la première occurrence
 *        Les valeurs dans le tableau doivent rester contigues : les cases vides "déplacées" à la fin et remplies par des -1.
 * @param array Tableau d'entiers
 * @param size Taille du tableau
 * @return le nombre de valeurs supprimées du tableau
 */
int simplify(int array[], int size);

/**
 * @brief Découpe un tableau donné en deux parties.
 *        Les éléments du tableau d'entrée doivent être copiés soit dans le premier tableau résultat, soit dans le second.
 *        Si l'indice est hors du tableau d'entrée, le découpage se fait en copiant toutes les valeurs dans le même tableau résultat.
 * @param array Le tableau à découper, ne sera pas modifié par la fonction
 * @param size Taille du tableau à découper
 * @param index Indice auquel découper le tableau. Indique le premier élément du second tableau
 * @param left Premier tableau résultat, de la même taille que array et rempli de 0
 * @param right Second tableau résultat, de la même taille que array et rempli de 0
 * @return 0 si les deux tableaux résultats sont non vides ou si le tableau d'entrée est vide
 *         1 si right est vide
 *         -1 si left est vide
 */
int split(int array[], int size, int index, int left[], int right[]);

}    // namespace ArrayExercices
