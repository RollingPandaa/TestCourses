#include <QCoreApplication>
#include <QtTest>

// add necessary includes here
#include "BasicExercices.h"
#include "SortingExercices.h"

class BasicTest : public QObject
{
    Q_OBJECT

public:
    BasicTest();
    ~BasicTest();

private slots:
    void initTestCase();
    void cleanupTestCase();
    /*
    void test_triangularNumber_data();
    void test_triangularNumber();

    void test_relativeTriangularNumber_data();
    void test_relativeTriangularNumber();

    void test_trapezoidNumber_data();
    void test_trapezoidNumber();

    void test_factorial_data();
    void test_factorial();

    void test_search_data();
    void test_search();

    void test_searchString_data();
    void test_searchString();
//*/
    void test_swap_data();
    void test_swap();

    void test_sort_data();
    void test_sort();

private:
    int *m_tab;
};

BasicTest::BasicTest()
    : m_tab{nullptr}
{}

BasicTest::~BasicTest()
{
    delete[] m_tab;
}

void BasicTest::initTestCase() {}

void BasicTest::cleanupTestCase() {}
/*
void BasicTest::test_triangularNumber_data()
{
        QTest::addColumn<int>("number");
        QTest::addColumn<long long>("result");

        QTest::newRow("1") << 1 << 1ll;
        QTest::newRow("7") << 7 << 28ll;
        QTest::newRow("12") << 12 << 78ll;
        QTest::newRow("10") << 10 << 55ll;
        QTest::newRow("32") << 32 << 528ll;
        QTest::newRow("0") << 0 << 0ll;
        QTest::newRow("1024") << 1024 << 524800ll;
        QTest::newRow("2154876") << 2154876 << 2321746365126ll;
}

void BasicTest::test_triangularNumber()
{
        QFETCH(int, number);
        QFETCH(long long, result);
        QCOMPARE(BasicExercices::triangularNumber(number), result);
}

void BasicTest::test_relativeTriangularNumber_data()
{
        QTest::addColumn<int>("number");
        QTest::addColumn<long long>("result");

        QTest::newRow("1") << 1 << 1ll;
        QTest::newRow("7") << 7 << 28ll;
        QTest::newRow("-12") << -12 << -78ll;
        QTest::newRow("10") << 10 << 55ll;
        QTest::newRow("-32") << -32 << -528ll;
        QTest::newRow("0") << 0 << 0ll;
        QTest::newRow("-1024") << -1024 << -524800ll;
        QTest::newRow("-2154876") << -2154876 << -2321746365126ll;
}

void BasicTest::test_relativeTriangularNumber()
{
        QFETCH(int, number);
        QFETCH(long long, result);
        QCOMPARE(BasicExercices::relativeTriangularNumber(number), result);
}

void BasicTest::test_trapezoidNumber_data()
{
        QTest::addColumn<int>("mini");
        QTest::addColumn<int>("maxi");
        QTest::addColumn<long long>("result");

        QTest::newRow("3 & 5") << 3 << 5 << 12ll;
        QTest::newRow("5 & 3") << 5 << 3 << 12ll;
        QTest::newRow("-5 & -3") << -5 << -3 << -12ll;
        QTest::newRow("-3 & -5") << -3 << -5 << -12ll;
        QTest::newRow("-3 & 3") << -3 << 3 << 0ll;
        QTest::newRow("-3 & 5") << -3 << 5 << 9ll;
        QTest::newRow("5 & -3") << 5 << -3 << 9ll;
        QTest::newRow("-5 & 3") << -5 << 3 << -9ll;
        QTest::newRow("3 & -5") << 3 << -5 << -9ll;
        QTest::newRow("3 & -5") << 3 << -5 << -9ll;
        QTest::newRow("12489 & -654872") << 12489 << -654872 << -214351001823ll;
}

void BasicTest::test_trapezoidNumber()
{
        QFETCH(int, mini);
        QFETCH(int, maxi);
        QFETCH(long long, result);
        QCOMPARE(BasicExercices::trapezoidNumber(mini, maxi), result);
}

void BasicTest::test_factorial_data()
{
        QTest::addColumn<int>("number");
        QTest::addColumn<long long>("result");

        QTest::newRow("0") << 0 << 1ll;
        QTest::newRow("1") << 1 << 1ll;
        QTest::newRow("4") << 4 << 24ll;
        QTest::newRow("7") << 7 << 5040ll;
        QTest::newRow("12") << 12 << 479001600ll;
}

void BasicTest::test_factorial()
{
        QFETCH(int, number);
        QFETCH(long long, result);
        QCOMPARE(BasicExercices::factorial(number), result);
}

void BasicTest::test_search_data()
{
        QTest::addColumn<int>("size");
        QTest::addColumn<int>("toFind");
        QTest::addColumn<int>("result");

        m_tab = new int[]{2, 7, 56, 35, 24, 784, 6948, 56, 1, 75};

        QTest::newRow("found") << 10 << 35 << 3;
        QTest::newRow("not found") << 10 << 4 << -1;
        QTest::newRow("two occurences") << 10 << 56 << 2;
        QTest::newRow("empty tab") << 0 << 1 << -1;
}

void BasicTest::test_search()
{
        QFETCH(int, size);
        QFETCH(int, toFind);
        QFETCH(int, result);

        QCOMPARE(BasicExercices::search(toFind, size, m_tab), result);
}

void BasicTest::test_searchString_data()
{
        QTest::addColumn<QString>("base");
        QTest::addColumn<QString>("toFind");
        QTest::addColumn<int>("result");

        QTest::newRow("found") << "exemple substring found" << "substring" << 8;
        QTest::newRow("at start") << "exemple substring found" << "exe" << 0;
        QTest::newRow("last word") << "exemple substring found" << " found" << 17;
        QTest::newRow("not found") << "exemple substring found" << "sub string" << -1;
        QTest::newRow("end cutted") << "exemple substring fou" << "found" << -1;
        QTest::newRow("one char") << "exemple substring found" << "u" << 9;
        QTest::newRow("empty to found") << "exemple substring found" << "" << -1;
        QTest::newRow("empty base") << "" << "substring" << -1;
}

void BasicTest::test_searchString()
{
        QFETCH(QString, base);
        QFETCH(QString, toFind);
        QFETCH(int, result);

        QCOMPARE(BasicExercices::searchString(base.toStdString().c_str(),
                                              toFind.toStdString().c_str()),
                 result);
}
//*/

void BasicTest::test_swap_data()
{
        QTest::addColumn<QList<int>>("list");
        QTest::addColumn<int>("fromIndex");
        QTest::addColumn<int>("toIndex");
        QTest::addColumn<QList<int>>("swapped");

        QTest::newRow("simple") << QList<int>({44, 9, 16, 7, 21, 48, 56, 48, 78}) << 2 << 5
                                << QList<int>({44, 9, 48, 7, 21, 16, 56, 48, 78});
        QTest::newRow("revert") << QList<int>({44, 9, 16, 7, 21, 48, 56, 48, 78}) << 5 << 2
                                << QList<int>({44, 9, 48, 7, 21, 16, 56, 48, 78});
        QTest::newRow("short") << QList<int>({44, 9}) << 0 << 1 << QList<int>({9, 44});
        QTest::newRow("empty") << QList<int>() << 5 << 9 << QList<int>();
        QTest::newRow("not swap") << QList<int>({44, 9, 16, 7, 21, 48, 56, 48, 78}) << 3 << 3
                                  << QList<int>({44, 9, 16, 7, 21, 48, 56, 48, 78});
        QTest::newRow("out of bound") << QList<int>({44, 9, 16, 7, 21, 48, 56, 48, 78}) << 4 << 56
                                      << QList<int>({44, 9, 16, 7, 21, 48, 56, 48, 78});
}

void BasicTest::test_swap()
{
        QFETCH(QList<int>, list);
        QFETCH(int, fromIndex);
        QFETCH(int, toIndex);
        QFETCH(QList<int>, swapped);

        QVERIFY2(list.size() == swapped.size(), "Test data error : the list and swapped must have the same size");

        int array[list.size()];
        int result[swapped.size()];
        for (int i = 0; i < list.size(); ++i) {
            array[i] = list.at(i);
            result[i] = swapped.at(i);
        }

        SortingExercices::swapInPlace(fromIndex, toIndex, array, list.size());

        for (int i = 0; i < list.size(); ++i)
            QCOMPARE(array[i], result[i]);
}

void BasicTest::test_sort_data()
{
        QTest::addColumn<SortingExercices::Algorithm>("algo");
        QTest::addColumn<QList<int>>("initial");

        for (SortingExercices::Algorithm a = SortingExercices::MinAlgo; a <= SortingExercices::MaxAlgo; a = SortingExercices::Algorithm(int(a) + 1))
        {
            QString key(QMetaEnum::fromType<SortingExercices::Algorithm>().valueToKey(a));

            QTest::newRow((key + " simple").toLatin1().data()) << a << QList<int>({44, 9, 16, 7, 21, 48, 56, 78, 12, 35, 1457, 435, 864, 1, 457});
            QTest::newRow((key + " with double").toLatin1().data()) << a << QList<int>({44, 9, 16, 435, 7, 21, 48, 56, 78, 12, 35, 1457, 435, 21, 864, 1, 457});
            QTest::newRow((key + " empty").toLatin1().data()) << a << QList<int>();
            QTest::newRow((key + " unique").toLatin1().data()) << a << QList<int>({7});
            QTest::newRow((key + " little").toLatin1().data()) << a << QList<int>({21, 14});
            QTest::newRow((key + " little sorted").toLatin1().data()) << a << QList<int>({14, 21});
            QTest::newRow((key + " sorted").toLatin1().data()) << a << QList<int>({1, 7, 9, 12, 16, 21, 21, 35, 44, 48, 56, 78, 435, 435, 457, 864, 1457});
            QTest::newRow((key + " equals").toLatin1().data()) << a << QList<int>({7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7});
            QTest::newRow((key + " reverting").toLatin1().data()) << a << QList<int>({1457, 864, 487, 435, 78, 56, 48, 44, 35, 21, 16, 12, 9, 7, 1});
            QTest::newRow((key + " palindrom").toLatin1().data()) << a << QList<int>({1, 7, 9, 12, 16, 21, 35, 44, 48, 56, 78, 435, 78, 56, 48, 44, 35, 21, 16, 12, 9, 7, 1});
        }
}

void BasicTest::test_sort()
{
        QFETCH(SortingExercices::Algorithm, algo);
        QFETCH(QList<int>, initial);

        int size(initial.size());
        int array[size];
        for (int i = 0; i < size; ++i)
            array[i] = initial.at(i);

        SortingExercices::sortInPlace(array, size, algo);

        QList<int> final;

        for (int i = 0; i < size; ++i)
            final.append(array[i]);

        std::sort(initial.begin(), initial.end());

        QCOMPARE(final, initial);
}

QTEST_MAIN(BasicTest)

#include "tst_basictest.moc"
