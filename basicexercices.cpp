#include "basicexercices.h"

#include <QtGlobal>
#include <cmath>

long long BasicExercices::triangularNumber(int number)
// Calcule la somme des nombres entiers de 0 à number
// (1 + 2 + 3 + ... + number)
// Avec number >= 0
// Il existe plusieurs solutions possibles
{
    return -1;
}

long long BasicExercices::relativeTriangularNumber(int number)
// Calcule la somme des nombres entiers de 0 à number
// Number peut avoir n"importe quelle valeur entière
// Il existe plusieurs solutions possibles
{
    return 0;
}

long long BasicExercices::trapezoidNumber(int n1, int n2)
//	Calcule la somme des nombres entiers de n1 à n2
// Il existe plusieurs solutions possibles
{
    return -2;
}

long long BasicExercices::factorial(int nb)
//	Calcule le facteur des nombres entiers de 1 à nb
//	Si nb < 0, retourne -1
//  (Rappel : factorial(0) => 1)
{
    return -2;
}

int BasicExercices::search(int toFind, int size, int *tab)
//	Dans un tableau tab de size valeurs
// retourne l'indice de la valeur toFind
// si le tableau ne contient aucune occurence de toFind, retourne -1
{
    return -2;
}

int BasicExercices::searchString(const char *string, const char *toFind)
// Recherche la première occurence d'une chaîne de caractères dans une autre
// Retourne l'indice dans string de la première occurence de toFind
// Si l'une des deux chaînes est vide ou si rien n'est trouvé, retourne -1
// Les deux chaînes sont terminées par '\0'
{
    return -1;
}

/*
void BasicExercices::bubbleSort(int size, int *tab)
//	Trie les valeurs de tab en ordre croissant en utilise l'algoritme de tri à bulles
//	tab contient size valeurs
{}

int BasicExercices::dichotomicSearch(int toFind, int size, int *tab)
//	Dans un tableau tab de size valeurs
// retourne l'indice de la valeur toFind, effectuer une recherche par dichotomie
// si le tableau ne contient aucune occurence de toFind, retourne -1
{
    return -1;
}
*/
