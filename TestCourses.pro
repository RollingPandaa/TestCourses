QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG += no_testcase_installs
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_basictest.cpp \
    ArrayExercices.cpp \
    BasicExercices.cpp \
    SortingExercices.cpp

HEADERS += \
    ArrayExercices.h \
    BasicExercices.h \
    SortingExercices.h
