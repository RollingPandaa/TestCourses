
#include "ArrayExercices.h"

bool ArrayExercices::swapInPlace(int fromIndex, int toIndex, int array[], int size) {}

int ArrayExercices::searchFirst(int val, int array[], int size) {}

int ArrayExercices::searchLast(int val, int array[], int size) {}

int ArrayExercices::search(int val, int array[], int size, bool last) {}

int ArrayExercices::removeFirst(int val, int array[], int size) {}

int ArrayExercices::removeAll(int val, int array[], int size) {}

int ArrayExercices::simplify(int array[], int size) {}

int ArrayExercices::split(int array[], int size, int index, int left[], int right[]) {}
