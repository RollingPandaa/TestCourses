#ifndef BASICEXERCICES_H
#define BASICEXERCICES_H


class BasicExercices
{
public:
    static long long triangularNumber(int number);

    static long long relativeTriangularNumber(int number);

    static long long trapezoidNumber(int n1, int n2);

    static long long factorial(int nb);

    static void ascendingSortInPlace(int size, int *tab);

    static int search(int toFind, int size, int *tab);

    static int searchString(const char *string, const char *toFind);

private:
    static void bubbleSort(int size, int *tab);

    static int dichotomicSearch(int toFind, int size, int* tab);
};

#endif // BASICEXERCICES_H
