
#include "SortingExercices.h"

void SortingExercices::sortInPlace(int array[], int size, Algorithm algo)
{
    switch (algo)
    {
        case QuickSort: quickSort(array, size); break;
        case MergeSort: mergeSort(array, size); break;
        case IntroSort: introSort(array, size); break;
        case HeapSort: heapSort(array, size); break;
        case InsertionSort: insertionSort(array, size); break;
        case BlockSort: blockSort(array, size); break;
        case TimSort: timSort(array, size); break;
        case SelectionSort: selectionSort(array, size); break;
        case CubeSort: cubeSort(array, size); break;
        case ShellSort: shellSort(array, size); break;
        case BubbleSort: bubbleSort(array, size); break;
        case ExchangeSort: exchangeSort(array, size); break;
        case TreeSort: treeSort(array, size); break;
        case CycleSort: cycleSort(array, size); break;
        case LibrarySort: librarySort(array, size); break;
        case PatienceSort: patienceSort(array, size); break;
        case SmoothSort: smoothSort(array, size); break;
        case StrandSort: strandSort(array, size); break;
        case TournamentSort: tournamentSort(array, size); break;
        case ShakerSort: shakerSort(array, size); break;
        case CombSort: combSort(array, size); break;
        case GnomeSort: gnomeSort(array, size); break;
        case OddEvenSort: oddEvenSort(array, size); break;
        default: break;
    }
}

// Pour toutes les fonctions ci-dessous :
// Tire les valeurs du tableau en place dans l'ordre ascendant
// En utilisant l'algorithme indiqué par le nom de la fonction

void SortingExercices::quickSort(int array[], int size)
{
    if (size == 2 && array[0] > array[1])
        swapInPlace(0, 1, array, size);
    else if (size > 2)
    {
        int indexDown(0), indexUp(size - 2);
        int pile(array[size / 2]);
        swapInPlace(size / 2, size - 1, array, size);

        while (indexDown < size && indexUp > indexDown)
        {
            if (array[indexDown] >= pile)
            {
                swapInPlace(indexDown, indexUp, array, size);
                indexUp--;
            }
            else
                indexDown++;
        }

        if (array[indexDown] < pile)
            indexDown++;

        swapInPlace(size - 1, indexDown, array, size);

        quickSort(array, indexDown);
        quickSort(&array[indexDown + 1], size - indexDown - 1);
    }
}

void SortingExercices::bubbleSort(int array[], int size)
{
    int limit(size), index(0);

    while (limit > 0 && index < limit)
    {
        if (array[limit] < array[index])
            swapInPlace(limit, index, array, size);

        index++;
        if (index >= limit)
        {
            index = 0;
            limit--;
        }
    }
}

void SortingExercices::mergeSort(int array[], int size)
{
    if (size == 2 && array[0] > array[1])
        swapInPlace(0, 1, array, size);
    else if (size > 2)
    {
        int middle(size / 2);

        int firstPart[middle], secondPart[size - middle - 1];
        memcpy(firstPart, array, middle);
        memcpy(secondPart, &array[middle], size - middle - 1);

        mergeSort(firstPart, middle);
        mergeSort(secondPart, size - middle - 1);

        int index(0), firstIndex(0), secondIndex(middle);
        while (firstIndex < middle && secondIndex < size)
        {
            if (firstPart[firstIndex] <= secondPart[secondIndex])
            {
                array[index]     = firstPart[firstIndex];
                array[index + 1] = secondPart[secondIndex];
            }
            else
            {
                array[index]     = secondPart[secondIndex];
                array[index + 1] = firstPart[firstIndex];
            }
            index += 2;
        }

        while (firstIndex < middle)
        {
            array[index] = firstPart[firstIndex];
            index++;
        }

        while (secondIndex < size)
        {
            array[index] = secondPart[secondIndex];
            index++;
        }
    }
}

void SortingExercices::introSort(int array[], int size) {}

void SortingExercices::heapSort(int array[], int size) {}

void SortingExercices::insertionSort(int array[], int size) {}

void SortingExercices::blockSort(int array[], int size) {}

void SortingExercices::timSort(int array[], int size) {}

void SortingExercices::selectionSort(int array[], int size) {}

void SortingExercices::cubeSort(int array[], int size) {}

void SortingExercices::shellSort(int array[], int size) {}

void SortingExercices::exchangeSort(int array[], int size) {}

void SortingExercices::treeSort(int array[], int size) {}

void SortingExercices::cycleSort(int array[], int size) {}

void SortingExercices::librarySort(int array[], int size) {}

void SortingExercices::patienceSort(int array[], int size) {}

void SortingExercices::smoothSort(int array[], int size) {}

void SortingExercices::strandSort(int array[], int size) {}

void SortingExercices::tournamentSort(int array[], int size) {}

void SortingExercices::shakerSort(int array[], int size) {}

void SortingExercices::combSort(int array[], int size) {}

void SortingExercices::gnomeSort(int array[], int size) {}

void SortingExercices::oddEvenSort(int array[], int size) {}
